
import { SkDatepickerImpl }  from "../../sk-datepicker/src/impl/sk-datepicker-impl.js";

import { DateFormat, DateParse, DateTime } from "../../dateutils/src/global.js";
import { CALENDAR_NO_ICON_AN } from "../../sk-datepicker/src/sk-datepicker.js";


export class JquerySkDatepicker extends SkDatepickerImpl {

    get prefix() {
        return 'jquery';
    }

    get suffix() {
        return 'datepicker';
    }

    get input() {
        if (! this._input) {
            this._input = this.comp.el.querySelector('input');
        }
        return this._input;
    }

    set input(el) {
        this._input = el;
    }

    get picker() {
        if (! this._picker) {
            this._picker = this.comp.el.querySelector('input');
        }
        return this._picker;
    }

    set picker(el) {
        this._picker = el;
    }

    get pickerContainer() {
        if (! this._pickerContainer) {
            this._pickerContainer = this.comp.el.querySelector('#ui-datepicker-div');
        }
        return this._pickerContainer;
    }

    set pickerContainer(el) {
        this._pickerContainer = el;
    }

    get calendarTBody() {
        if (! this._calendarTBody) {
            this._calendarTBody = this.comp.el.querySelector('table.ui-datepicker-calendar tbody');
        }
        return this._calendarTBody;
    }

    set calendarTBody(el) {
        this._calendarTBody = el;
    }

    get calendarTHead() {
        if (! this._calendarTHead) {
            this._calendarTHead = this.comp.el.querySelector('table.ui-datepicker-calendar thead');
        }
        return this._calendarTHead;
    }

    set calendarTHead(el) {
        this._calendarTHead = el;
    }

    get curMonthLabel() {
        if (! this._curMonthLabel) {
            this._curMonthLabel = this.comp.el.querySelector('.ui-datepicker-month');
        }
        return this._curMonthLabel;
    }

    set curMonthLabel(el) {
        this._curMonthLabel = el;
    }

    get curYearLabel() {
        if (! this._curYearLabel) {
            this._curYearLabel = this.comp.el.querySelector('.ui-datepicker-year');
        }
        return this._curYearLabel;
    }

    set curYearLabel(el) {
        this._curYearLabel = el;
    }

    get prevMonthBtn() {
        if (! this._prevMonthBtn) {
            this._prevMonthBtn = this.comp.el.querySelector('.ui-datepicker-prev');
        }
        return this._prevMonthBtn;
    }

    set prevMonthBtn(el) {
        this._prevMonthBtn = el;
    }

    get selectMonthBtn() {
        if (! this._selectMonthBtn) {
            this._selectMonthBtn = this.comp.el.querySelector('.ui-datepicker-month');
        }
        return this._selectMonthBtn;
    }

    set selectMonthBtn(el) {
        this._selectMonthBtn = el;
    }

    get selectYearBtn() {
        if (! this._selectYearBtn) {
            this._selectYearBtn = this.comp.el.querySelector('.ui-datepicker-year');
        }
        return this._selectYearBtn;
    }

    set selectYearBtn(el) {
        this._selectYearBtn = el;
    }

    get nextMonthBtn() {
        if (! this._nextMonthBtn) {
            this._nextMonthBtn = this.comp.el.querySelector('.ui-datepicker-next');
        }
        return this._nextMonthBtn;
    }

    get calendarInput() {
        if (! this._calendarInput) {
            this._calendarInput = this.comp.el.querySelector('input');
        }
        return this._calendarInput;
    }

    set calendarInput(el) {
        this._calendarInput = el;
    }

    get currentYear() {
        return this._currentYear || DateTime.today().getFullYear();
    }

    set currentYear(el) {
        this._currentYear = el;
    }

    get currentMonth() {
        return this._currentMonth || DateTime.today().getMonth();
    }

    set currentMonth(el) {
        this._currentMonth = el;
    }

    get subEls() {
        return [ 'input', 'picker', 'pickerContainer', 'calendarTBody', 'calendarTHead', 'curMonthLabel', 'curYearLabel',
            'prevMonthBtn', 'selectMonthBtn', 'selectYearBtn', 'calendarInput', 'currentYear', 'currentMonth' ];
    }

    renderImpl() {
        let themeLoaded = this.initTheme();
        themeLoaded.finally(() => { // we just need to try loading styles
            this.comp.tpl = this.comp.renderer.findTemplateEl(this.cachedTplId, this.comp); // try to find overriden template
            if (!this.comp.tpl) {
                const loadTpl = async () => {
                    this.comp.tpl = await this.comp.renderer.mountTemplate(this.tplPath, this.comp.constructor.name + 'Tpl',
                        this.comp, {
                            themePath: this.themePath
                        });
                };
                loadTpl().then(() => {
                    this.doRender();
                });
            } else {
                this.doRender();
            }
        });

    }

    updateInputValue() {
        this.calendarInput.value = this.comp.value;
        this.comp.dispatchEvent(new CustomEvent('input', { target: this, bubbles: true, composite: true, detail: { }}));
        this.comp.dispatchEvent(new CustomEvent('skinput', { target: this, bubbles: true, composite: true, detail: { }}));
    }

    renderPickerLabel() {
        this.curMonthLabel.innerHTML = DateFormat.format(DateTime.fromDate(this.currentYear, this.currentMonth, 1), 'F', this.comp.dateLocale);
        this.curMonthLabel.setAttribute('ref', this.currentMonth);
        this.curYearLabel.innerHTML = this.currentYear;
    }

    renderDays() {
        this.calendarTBody.innerHTML = '';
        let firstMonthDay = DateTime.fromDate(this.currentYear, this.currentMonth, 1);

        let firstWeekDay = firstMonthDay.getFirstDateOfWeek(this.comp.dateLocale);
        for (let w = 0; w <= 5; w++) { // weeks as displayed in picker
            let weekRow = this.comp.renderer.createEl('tr');
            weekRow.setAttribute('role', 'row');
            for (let i = 1; i <= 7; i++) {
                let dayOffset = ((w * 7) + i) - 1;
                let day = firstWeekDay.plusDays(dayOffset);
                let dayCell = day.getMonth() === firstMonthDay.getMonth()  ?
                    `
                        <td class="calendar-date " data-handler="selectDay" data-event="click" data-month="${day.getMonth()}" data-year="${day.getFullYear()}" data-day="${day.getDate()}" >
                            <a class="ui-state-default">${DateFormat.format(day, 'j', this.comp.dateLocale)}</a>
                        </td>
                    `
                    :
                    `<td class=" ui-datepicker-week-end ui-datepicker-other-month ui-datepicker-unselectable ui-state-disabled">&nbsp;</td>`
                ;
                weekRow.insertAdjacentHTML('beforeend', dayCell);
            }
            this.calendarTBody.appendChild(weekRow);
        }
    }

    renderWeekDaysHeading() {
        let wdRow = this.comp.renderer.createEl('tr');
        wdRow.setAttribute('role', 'row');
        let firstWdDay = DateTime.today().getFirstDateOfWeek(this.comp.dateLocale);
        for (let wd = 0; wd <= 6; wd++) { // weekday code heading
            let weekDay = firstWdDay.plusDays(wd);
            let wdCell =
                `
                    <th scope="col"><span title="${DateFormat.format(weekDay, 'D', this.comp.dateLocale)}"</span></th>
                `
            ;
            wdRow.insertAdjacentHTML('beforeend', wdCell);
        }
        this.calendarTHead.appendChild(wdRow);
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
        this.pickerContainer.style.display = 'none';
        this.picker.addEventListener('focusin', (event) => {
            this.showPicker();
        });
        this.pickerContainer.addEventListener('focusin', (event) => {
            this.showPicker();
        });
        this.pickerContainer.addEventListener('focusout', (event) => {
            this.closePicker();
        });
        this.onScrollHandler = this.fitPicker.bind(this);
        window.addEventListener('scroll', this.onScrollHandler);
        this.renderPickerIcon();
        this.mountStyles();
    }
    
    renderPickerIcon() {
        if (! this.comp.hasAttribute(CALENDAR_NO_ICON_AN)) {
            this.picker.insertAdjacentHTML('afterend',
                `<img class="ui-datepicker-trigger" src="${this.themePath}/images/calendar.gif" alt="Select date" title="Select date" />`);
        }
    }

    closePicker() {
        this.pickerContainer.style.display = 'none';
        this.comp.removeAttribute('open');
    }

    showPicker() {
        this.fitPicker(true);
        this.pickerContainer.style.display = 'block';
        this.comp.setAttribute('open', '');
    }

    fitPicker(force) {
        if (force || this.comp.hasAttribute('open')) {
            let box = this.picker.getBoundingClientRect();
            this.pickerContainer.style.position = 'fixed';
            this.pickerContainer.style.top = `${(window.pageYOffset + box.bottom) - this.getScrollTop()}px`;
            this.pickerContainer.style.left = `${box.left}px`;
        }
    }

    selectDay(dayValue) {
        let day = DateTime.fromDate(this.currentYear, this.currentMonth, dayValue);
        this.comp.value = DateFormat.format(day, this.comp.fmt, this.dateLocale);
        this.updateInputValue();
    }

    bindEvents() {
        super.bindEvents();
        this.comp.el.addEventListener('click', (event) => {
            let targetEl = event.target;
            let currentDate = DateTime.fromDate(this.currentYear, this.currentMonth, 1);
            if (targetEl) {
                if (targetEl.parentElement && targetEl.parentElement.classList.contains('calendar-date')) {
                    let dayValue = parseInt(event.target.innerText);
                    this.selectDay(dayValue);
                    this.closePicker();
                } else if (targetEl === this.selectMonthBtn) {
                    this.currentMonth = parseInt(event.target.getAttribute('ref'));
                    let date = DateParse.parseDate(this.input.value, this.comp.fmt);
                    this.selectDay(date.getDay());
                    this.closePicker();
                } else if (targetEl === this.selectYearBtn) {
                    this.currentYear = parseInt(event.target.innerText);
                    let date = DateParse.parseDate(this.input.value, this.comp.fmt);
                    this.selectDay(date.getDay());
                    this.closePicker();
                } else if (targetEl.parentElement && targetEl.parentElement === this.prevMonthBtn) {
                    if (this.currentMonth > 1 && this.currentMonth <= 12) {
                        currentDate = DateTime.fromDate(this.currentYear, this.currentMonth - 1, 1);
                    } else {
                        if (this.currentMonth <= 1) {
                            currentDate = DateTime.fromDate(this.currentYear - 1, 12, 1);
                        }
                    }
                } else if (targetEl.parentElement && targetEl.parentElement === this.nextMonthBtn) {
                    if (this.currentMonth >= 1 && this.currentMonth < 12) {
                        currentDate = DateTime.fromDate(this.currentYear, this.currentMonth + 1, 1);
                    } else {
                        if (this.currentMonth >= 12) {
                            currentDate = DateTime.fromDate(this.currentYear + 1, 1, 1);
                        }
                    }
                } else if (targetEl.parentElement && targetEl.parentElement === this.prevYearBtn) {
                    currentDate = DateTime.fromDate(this.currentYear - 1, this.currentMonth, 1);
                } else if (targetEl.parentElement && targetEl.parentElement === this.nextYearBtn) {
                    currentDate = DateTime.fromDate(this.currentYear + 1, this.currentMonth, 1);
                } else if (targetEl.parentElement && targetEl.parentElement === this.todayBtn) {
                    let now = DateTime.today();
                    this.currentYear = now.getFullYear();
                    this.currentMonth = now.getMonth();
                    this.selectDay(now.getDate());
                    this.closePicker();
                } else if (targetEl.classList.contains('ui-datepicker-trigger')) {
                    if (this.comp.hasAttribute('open')) {
                        this.closePicker();
                    } else {
                        this.showPicker();
                    }
                }
                this.currentYear = currentDate.getFullYear();
                this.currentMonth = currentDate.getMonth();
                this.renderPickerLabel();
                this.renderDays();
            }
        })
    }

}
